const pool = require('./db_config'),
    secret = require('./encryption_config'),
    saltRounds = 10

const passport = require('passport'),
    JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt,
    LocalStrategy = require('passport-local').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    TwitterStrategy = require('passport-twitter').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    bcrypt = require('bcrypt'),
    social_login_config = require('./social_login_config')


var opts = {}
opts.jwtFromRequest = ExtractJwt.fromHeader("access-token");    // jwt will be extracted from header('access-token')
opts.secretOrKey = secret.secret_key;                                      //encryption secret key
passport.use('verify-jwt', new JwtStrategy(opts, function (jwt_payload, done) {
    done(null, jwt_payload)
}));


passport.use('signup', new LocalStrategy({
    usernameField: "email",
    passwordField: "",
    session: false,

},
    function (username, password, done) {
        const query = {
            name: 'verify that email is unique',
            text: 'SELECT email FROM Website_Users WHERE email = $1',
            values: [username]
        }
        pool.query(query)
            .then(res => {
                if (res.rows.length == 0)
                    done(null, true)     //can add this user to database
                else
                    done(null, false)   // 401 response code mean the email already exists in table
            })
            .catch(error => { done(error, false) })

    }
));

passport.use('signin', new LocalStrategy({
    usernameField: "email",
    passwordField: "password",
    session: false
},
    function (email, password, done) {
        const query = {
            name: 'Select  user from database',
            text: 'SELECT password from Website_Users WHERE email=$1 ',
            values: [email]
        }
        pool.query(query)
            .then(response => {
                var api_response = {}
                if (response.rows.length > 0) {
                    bcrypt.compare(password, response.rows[0].password).then(function (res) {
                        if (res)
                            done(null, true)
                        else
                            done(null, false)
                    });
                }
            })
            .catch(error => { done(error, false) })
        bcrypt.hash(password, saltRounds).then(function (hash) {


        });
    }
));


passport.use(new FacebookStrategy(social_login_config.facebook_config,
    function (accessToken, refreshToken, profile, done) {
        verify_uniqueness(profile.emails[0].value, profile.name.givenName, profile.name.familyName, profile, done)
    }
));

passport.use(new TwitterStrategy(social_login_config.twitter_config,
    function (token, tokenSecret, profile, done) {
        var names = profile.displayName.split(' ');
        verify_uniqueness(profile.emails[0].value, names[0], names[1], profile, done)
    }
));

passport.use(new GoogleStrategy(social_login_config.google_config,
    function (accessToken, refreshToken, profile, done) {
        verify_uniqueness(profile.emails[0].value, profile.name.givenName, profile.name.familyName, profile, done)

    }
));

function verify_uniqueness(email, first_name, last_name, profile, done) {
    if (email == undefined) {
        console.log("plesae allow to access user email")
        done(null, false)
        return
    }
    const query = {
        name: 'verify that email is unique',
        text: 'SELECT email,login_type FROM Website_Users WHERE email = $1',
        values: [email]
    }
    pool.query(query)
        .then(res => {
            if (res.rows.length == 0)
                insert_social_users(email, first_name, last_name, profile, done)  //can add this user to database

            else if (res.rows[0].login_type == 'social')
                done(null, true, profile)           // user with this email already exist in db with login_type = "social"  

            else
                done(null, false)   // user with same email alrady exist via custom login
            // 401 response code mean the email already exists in table
        })
        .catch(error => { done(error, false) })

}

function insert_social_users(email, first_name, last_name, profile, done) {
    const query = {
        name: 'Insert new user in database',
        text: 'INSERT INTO Website_Users(email,first_name,last_name,password,login_type) VALUES($1,$2,$3,$4,$5)',
        values: [email, first_name, last_name, "", "social"],
    }
    pool.query(query)
        .then(response => {
            done(null, true, profile)  // return to callback
        })
        .catch(error => { done(null, false) })
}

module.exports = passport
