const passport = require('./passport_auth'),
    handler = require('./end_point_handler'),
    router = require('express').Router(),
    google_access_scope = require('./social_login_config').google_access_scope

router.post('/sign_up', passport.authenticate('signup', { session: false }), handler.signup_function)

router.post('/sign_in', passport.authenticate('signin', { session: false }), handler.signin_function)

router.post('/reset_password', passport.authenticate('verify-jwt', { session: false }), handler.reset_password_function)

router.get('/facebook', passport.authenticate('facebook'))           //url for facebook login
router.get('/twitter', passport.authenticate('twitter'));             //url for twitter login
router.get('/google', passport.authenticate('google', google_access_scope));  //url for google login


router.get('/auth/facebook/callback', passport.authenticate('facebook', { session: false, failureRedirect: '/social_login_failed' }), handler.facebook_calback);
router.get('/auth/twitter/callback', passport.authenticate('twitter', { session: false, failureRedirect: '/social_login_failed' }), handler.twitter_calback);
router.get('/auth/google/callback', passport.authenticate('google', { session: false, failureRedirect: '/social_login_failed' }), handler.google_calback);

router.get('/social_login_failed', handler.social_login_failed)      // url to handle failure calback

module.exports = router