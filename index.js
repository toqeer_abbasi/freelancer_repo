const http = require('http'),
    https = require('https'),
    bodyparser = require('body-parser'),
    express = require('express'),
    cors = require('cors'),
    session = require('express-session')
    cookieParser = require('cookie-parser')
    passport = require("./passport_auth"),
    https_config = require('./https_server_config'),         //path to https server configurations
    routes = require('./api_routes'),                        //path to the allowed api routes 
    secret_key=require('./encryption_config').secret_key                  
    pool = require('./db_config')                            //path to the database config

const app = express();
app.use(cookieParser(secret_key));
app.use(bodyparser.urlencoded({ extended: true }));
app.use(session({                                           //used to solve the twitter login issue
    resave: false,
    saveUninitialized: true,
    secret: secret_key
}));                                                        
app.use(passport.initialize());
app.use(passport.session());
app.use(cors())


app.use('/', routes)

const httpServer = http.createServer(app);
const httpsServer = https.createServer(https_config.credentials, app);

httpServer.listen(https_config.http_port_no, function () {
    console.log("http  server running on port " + https_config.http_port_no)

});

httpsServer.listen(https_config.https_port_no, function () {
    console.log("https  server running on port " + https_config.https_port_no)

});


