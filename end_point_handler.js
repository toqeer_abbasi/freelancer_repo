const bcrypt = require('bcrypt')
const validator = require('validator')
const pool = require('./db_config')
const secret = require('./encryption_config')
const jwt = require('jsonwebtoken');

var signup_function = function (req, res) {

  var is_mail = validate_email(req.body.email)
  var is_fisrt_name = validate_first_name(req.body.first_name)
  var is_last_name = validate_last_name(req.body.last_name)
  var is_password = validate_password(req.body.password)
  if (is_mail && is_fisrt_name && is_last_name && is_password) {
    bcrypt.hash(req.body.password, secret.saltRounds).then(function (hash) {
      const query = {
        name: 'Insert new user in database',
        text: 'INSERT INTO Website_Users(email,first_name,last_name,password,login_type) VALUES($1,$2,$3,$4,$5)',
        values: [req.body.email, req.body.first_name, req.body.last_name, hash,"custom"],
      }
      pool.query(query)
        .then(response => {
          var api_response = {}
          api_response.Message = "User Registeration Successfull"
          res.status(200).send(api_response)
        })
        .catch(error => { res.send({ Error: error }) })

    });
  } else {
    // create error response
    var api_response = {}
    if (!is_mail)
      api_response.mail = "please specify an email , max 30 characters"
    if (!is_last_name)
      api_response.last_name = "please specify last_name , 5-25 characters"
    if (!is_password)
      api_response.password = "please specify last_name , 5-20 characters"
    if (!is_fisrt_name)
      api_response.first_name = "please specify password , 5-25 characters"

    res.status(200).send(api_response)
  }
}

var signin_function = function (req, res) {
  const token = jwt.sign({ email: req.body.email }, secret.secret_key);
  res.status(200).send({ Message: "Login Successfull", "access-token": token })
}

var reset_password_function = function (req, res) {
  if (req.user.email == undefined) {
    res.status(200).send({ Message: "Cannot change Password of Social Media Platform" })
    return
  }
  else if (validate_password(req.body.password)) {
    bcrypt.hash(req.body.password, secret.saltRounds).then(function (hash) {
      const query = {
        name: 'Update password in database',
        text: 'UPDATE Website_Users SET password = $1 WHERE email = $2',
        values: [hash, req.user.email]
      }
      pool.query(query)
        .then(response => {
          var api_response = {}
          api_response.Message = "Password Updated Successfully"
          res.status(200).send(api_response)
        })
        .catch(error => { res.send({ Error: error }) })

    });
  } else
    res.status(200).send({ password: "please specify new password , 5-25 characters" })
}

var facebook_calback = function (req, res) {
  console.log("req.user")
  console.log(req.authInfo)
  const token = jwt.sign({ id: req.authInfo._json.email }, secret.secret_key);
  res.status(200).send({ Message: "Facebook Login Successfull", "access-token": token })
}

var google_calback = function (req, res) {
  const token = jwt.sign({ id: req.authInfo._json.email }, secret.secret_key);
  res.status(200).send({ Message: "Google Login Successfull", "access-token": token })
}

var twitter_calback = function (req, res) {
  console.log("req.user")
  console.log(req.authInfo)
  const token = jwt.sign({ id: req.authInfo._json.email }, secret.secret_key);
  res.status(200).send({ Message: "Twitter Login Successfull", "access-token": token })
}

var social_login_failed = function (req, res) {
  res.status(401).send({ Message: "User Canceled the Login Request" })
}


function validate_email(email) {
  return validator.isEmail(email) && email.length < 31
}

function validate_first_name(first_name) {
  return validator.isLength(first_name, { min: 5, max: 25 });
}

function validate_last_name(last_name) {
  return validator.isLength(last_name, { min: 5, max: 25 });

}

function validate_password(password) {
  return validator.isLength(password, { min: 5, max: 20 });
}

pool.connect((err, client, release) => {              // Create table if not exist
  if (!err && client !== null) {                      // Note : because of modifications in schema ... old table users need to signup again
    const queryText =
      `CREATE TABLE IF NOT EXISTS
        Website_Users (                               
          email VARCHAR(30) PRIMARY KEY,
          first_name VARCHAR(25) ,
          last_name VARCHAR(25)  ,
          password VARCHAR(60) ,
          login_type VARCHAR(10) NOT NULL
        )`;

    client.query(queryText)
      .then((res) => {  
      })
      .catch((err) => {
        console.log(err);
      });
  }
})

module.exports = {
  signup_function: signup_function,
  signin_function: signin_function,
  reset_password_function: reset_password_function,
  twitter_calback: twitter_calback,
  facebook_calback: facebook_calback,
  google_calback: google_calback,
  social_login_failed: social_login_failed
}