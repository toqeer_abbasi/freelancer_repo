secret_key = "bruteforcenotworking"  // encryption secret key
const saltRounds = 10                // need to adjust them accordingly
module.exports = {
    secret_key: secret_key,
    saltRounds: saltRounds
}