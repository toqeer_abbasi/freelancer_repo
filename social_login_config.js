const https_config = require("./https_server_config")       // Reference to server config file

const host = "http://localhost:"                           // set this as your domain name

const port = https_config.http_port_no;   //Important Note : In Production this line should be changed to =>  const port = https_config.https_port_no;             

const facebook_config = {
    clientID: 23615204172061,                           // FACEBOOK_APP_ID
    clientSecret: "a88ed42de139eb239f8bc3bda0522129",     //FACEBOOK_APP_SECRET
    callbackURL: host + port + "/auth/facebook/callback", // to set the success callback url
    profileFields: ['id', 'emails', 'name']
}

const twitter_config = {
    consumerKey: "dT9btYz6VE9xdHR6vlbRaR",                            // TWITTER_CONSUMER_KEY,
    consumerSecret: "3Nt63dv7VFXbew6xMnbfCByKuCY1alyenO2VkMIuExXYQtOx", //TWITTER_CONSUMER_SECRET,
    callbackURL: host + port + "/auth/twitter/callback",     // to set the success callback url 
    includeEmail: true
}

const google_config = {
    clientID: "470919068513-nascqg7b0f9f32cd700oan6v4l9hc.apps.googleusercontent.com", //GOOGLE_CLIENT_ID,
    clientSecret: "j4vU49fkHNA_R8c6hpbk",                                             // GOOGLE_CLIENT_SECRET,
    callbackURL: host + port + "/auth/google/callback"                                      // to set the success callback url
}

const google_access_scope = {
    scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
    ]
}
module.exports = {
    facebook_config: facebook_config,
    twitter_config: twitter_config,
    google_config: google_config,
    google_access_scope: google_access_scope
}