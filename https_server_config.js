const fs = require('fs');
const path = require('path')

const https_port_no = 3000;                             //https server to runn on port 3000
const http_port_no = 3001;

const certificate = fs.readFileSync(path.join(__dirname, 'certs', 'stag.crt'))
const key = fs.readFileSync(path.join(__dirname, 'certs', 'stag.key'))
const credentials = { key: key, cert: certificate }

module.exports = {
    credentials: credentials,
    http_port_no: http_port_no,
    https_port_no: https_port_no
}